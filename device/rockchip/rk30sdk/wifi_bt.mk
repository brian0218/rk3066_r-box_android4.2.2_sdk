#
# wifi bt chip config
#

# broadcom
BROADCOM_WIFI_SUPPORT := true
BROADCOM_BT_SUPPORT   := true

# mt5931 && mt6622
MT5931_WIFI_SUPPORT   := false
MT6622_BT_SUPPORT     := false

# rtl8723
RTL8723_BT_SUPPORT    := false

# rda5876
RDA587X_BT_SUPPORT   := false

# rtl8723au(USB interface)
RTL8723_BTUSB_SUPPORT := false

# mt7601U
MT7601U_WIFI_SUPPORT  := false
