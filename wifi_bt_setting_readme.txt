vim device/rockchip/rk30sdk/wifi_bt.mk
#
# wifi bt chip config
#

# broadcom
BROADCOM_WIFI_SUPPORT := false
BROADCOM_BT_SUPPORT   := false

# rtl8188&8189
RTL_WIFI_SUPPORT := true

# mt5931 && mt6622
MT5931_WIFI_SUPPORT   := false
MT6622_BT_SUPPORT     := false

# rtl8723
RTL8723_BT_SUPPORT    := false

# rda5876
RDA587X_BT_SUPPORT   := false

# rtl8723au(USB interface)
RTL8723_BTUSB_SUPPORT := false



mtk wifi and bt control gpio set

\kernel\arch\arm\mach-rk30\board-rk30-box.c

mtk bt gpio set
#if defined(CONFIG_MT5931_MT6622)
static struct mt6622_platform_data mt6622_platdata = 
           { .power_gpio         = { // BT_REG_ON        
                .io          = RK30_PIN0_PC6, // set io to INVALID_GPIO for disable it        
                .enable         = GPIO_HIGH,        
                .iomux          = {            
                      .name       = GPIO0C6_TRACECLK_SMCADDR2_NAME,            
                      .fgpio      = GPIO0C_GPIO0C6,        },    
             },    
             .reset_gpio         = { // BT_RST        
                  .io             = RK30_PIN4_PC5,        
                  .enable         = GPIO_LOW,        
                  .iomux          = {            
                      .name       = GPIO4C5_SMCDATA5_TRACEDATA5_NAME,            
                      .fgpio      = GPIO4C_GPIO4C5,         },    
             },    
             .irq_gpio           = {        
                  .io             = RK30_PIN3_PD2,        
                  .enable         = GPIO_HIGH,        
                  .iomux          = {            
                       .name       = GPIO3D2_SDMMC1INTN_NAME,            
                       .fgpio      = GPIO3D_GPIO3D2,        },    
             }
         };
#endif

mtk wifi gpio set
\kernel\arch\arm\mach-rk30\board-rk30-sdk-sdmmc.c
