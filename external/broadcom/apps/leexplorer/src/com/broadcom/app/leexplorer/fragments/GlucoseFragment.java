/******************************************************************************
 *
 *  Copyright (C) 2012 Broadcom Corporation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 ******************************************************************************/
package com.broadcom.app.leexplorer.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.broadcom.app.leexplorer.DataUtils;
import com.broadcom.app.leexplorer.R;
import com.broadcom.app.leexplorer.ValueFragment;

public class GlucoseFragment extends ValueFragment {
    private static final int FLAG_HAS_OFFSET = 0x01;
    private static final int FLAG_HAS_CONCENTRATION = 0x02;
    private static final int FLAG_UNIT_MOLL = 0x04;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_glucose, container, false);
    }

    @Override
    public void setValue(byte[] value) {
        if (value.length < 10) return;

        int offset = 0;
        int flags = DataUtils.unsignedByteToInt(value[offset++]);

        // Parse data

        int sequence = DataUtils.unsignedBytesToInt(value[offset++], value[offset++]);

        int ts_year = DataUtils.unsignedBytesToInt(value[offset++], value[offset++]);
        int ts_month = DataUtils.unsignedByteToInt(value[offset++]);
        int ts_day = DataUtils.unsignedByteToInt(value[offset++]);
        int ts_hour = DataUtils.unsignedByteToInt(value[offset++]);
        int ts_min = DataUtils.unsignedByteToInt(value[offset++]);
        int ts_sec = DataUtils.unsignedByteToInt(value[offset++]);

        int ts_offset = 0;
        if (isSet(flags, FLAG_HAS_OFFSET)) ts_offset = DataUtils.unsignedBytesToInt(value[offset++], value[offset++]);

        Float mgdl = 0F;
        if (isSet(flags, FLAG_HAS_CONCENTRATION)) {
            float concentration = DataUtils.bytesToFloat(value[offset++], value[offset++]);
            if (!isSet(flags, FLAG_UNIT_MOLL)) {
                mgdl = concentration * 100000; // Converts kg/l to mg/dl
            } else {
                mgdl = (concentration * 1000) * 18; // Converts mol/l > mg/dl
            }
        }

        // Enable measurement views

        setVisible(R.id.measurement);
        setVisible(R.id.textTimestamp);

        // Assign values

        String timestamp = String.format("%d: %02d:%02d:%02d %04d-%02d-%02d", sequence,
                                ts_hour, ts_min, ts_sec , ts_year, ts_month, ts_day);
        if (ts_offset > 0) timestamp += String.format(" +%d", ts_offset);

        setText(R.id.textRate, mgdl.toString());
        setText(R.id.textTimestamp, timestamp.toString());
     }

    private boolean isSet(int flags, int flag) {
        return ((flags & flag) != 0);
    }
}
